#See: https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/SoftwareTutorialxAODAnalysisInCMake for more details about anything here
import glob

#override next line on command line with: --filesInput=XXX
jps.AthenaCommonFlags.FilesInput = glob.glob('/global/cscratch1/sd/kkrizka/rucio/AOD/data18_13TeV.00360026.physics_EnhancedBias.recon.AOD.r13326/AOD.*.pool.root.1')

#Specify AccessMode (read mode) ... ClassAccess is good default for xAOD
jps.AthenaCommonFlags.AccessMode = "ClassAccess" 

# Run setup
from AthenaConfiguration.AutoConfigFlags import GetFileMD
from AthenaCommon.Logging import logging
log = logging.getLogger('ebntuples_jobOptions.py')

# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
from AnaAlgorithm.DualUseConfig import createService
from AnaAlgorithm.DualUseConfig import addPrivateTool

#
# BEING algorithm chain
#

sysService = createService( 'CP::SystematicsSvc', 'SystematicsSvc', sequence = athAlgSeq )
sysService.sigmaRecommended = 1

# Trigger
triggerChains = ['L1_2MU3V','L1_2MU5VF','L1_2MU8F','L1_BPH-2M9-0DR15-2MU3V','L1_BPH-0M16-15DR99-2MU3V']

from TriggerAnalysisAlgorithms.TriggerAnalysisSequence import \
    makeTriggerAnalysisSequence
triggerSequence = makeTriggerAnalysisSequence( 'mc', triggerChains=triggerChains )
athAlgSeq += triggerSequence

# L1 muon helper properties
lvl1MuonDecoder = createAlgorithm( 'LVL1MuonRoIDecoder', 'LVL1MuonRoIDecoder' )
lvl1MuonDecoder.InCollection ='LVL1MuonRoIs'
lvl1MuonDecoder.OutCollection='MyLVL1MuonRoIs'
athAlgSeq += lvl1MuonDecoder

# EB Weight tool
enhancedBiasWeighterAlgo = createAlgorithm( 'EnhancedBiasWeighterAlgo', 'EnhancedBiasWeighterAlgo' )
addPrivateTool( enhancedBiasWeighterAlgo, 'enhancedBiasTool', 'EnhancedBiasWeighter' )

enhancedBiasWeighterAlgo.enhancedBiasTool.RunNumber = 360026 #runNumbers[0]
enhancedBiasWeighterAlgo.enhancedBiasTool.UseBunchCrossingData = False
enhancedBiasWeighterAlgo.enhancedBiasTool.IsMC = False

athAlgSeq += enhancedBiasWeighterAlgo

# Tree dumper
treeMaker = createAlgorithm( 'CP::TreeMakerAlg', 'TreeMaker' )
athAlgSeq += treeMaker

ntupleMaker = createAlgorithm( 'CP::AsgxAODNTupleMakerAlg', 'NTupleMakerEventInfo' )
ntupleMaker.Branches = [ 'EventInfo.runNumber     -> runNumber',
                         'EventInfo.eventNumber   -> eventNumber',
                         'EventInfo.EBWeight      -> EBWeight']
ntupleMaker.Branches += ['EventInfo.trigPassed_' + t.replace('-','_') + ' -> trigPassed_' + t.replace('-','_') for t in triggerChains]
athAlgSeq += ntupleMaker

ntupleMaker = createAlgorithm( 'CP::AsgxAODNTupleMakerAlg', 'NTupleMakerMuons' )
ntupleMaker.Branches = [ 'Muons.eta -> mu_eta',
                         'Muons.phi -> mu_phi',
                         'Muons.pt  -> mu_pt' , ]
athAlgSeq += ntupleMaker

ntupleMaker = createAlgorithm( 'CP::AsgxAODNTupleMakerAlg', 'NTupleMakerL1Muons' )
ntupleMaker.Branches = [ 'MyLVL1MuonRoIs.thrValue  -> l1mu_thrValue' ,
                         'MyLVL1MuonRoIs.eta       -> l1mu_eta'      ,
                         'MyLVL1MuonRoIs.phi       -> l1mu_phi'      ,
                         'MyLVL1MuonRoIs.roiWord   -> l1mu_roiWord'  ,
                         'MyLVL1MuonRoIs.thrNumber -> l1mu_thrNumber',
                         'MyLVL1MuonRoIs.source    -> l1mu_source'   ,]
athAlgSeq += ntupleMaker

treeFiller = createAlgorithm( 'CP::TreeFillerAlg', 'TreeFiller' )
athAlgSeq += treeFiller


#
# END algorithm chain
#

jps.AthenaCommonFlags.HistOutputs = ["ANALYSIS:MyxAODAnalysis.outputs.root"]
svcMgr.THistSvc.MaxFileSize=-1

# optional include for reducing printout from athena
include("AthAnalysisBaseComps/SuppressLogging.py")
